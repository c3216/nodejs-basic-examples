var express = require('express');
var app = express();

app.get('/', function(req, res) {
  
    res.send('Hola Mundo! expres');
});

app.get ('/nombre/:id', function( req, res){
    var userId = req.params.id;

    console.log ( "accedeindo a nombre " + userId);
    res.send("Hola Mundo !  " + userId)
})

app.all('/secret', function(req, res, next) {

    console.log('Accediendo a la seccion secreta ...');
    res.send('secreto!');
    next(); 
  });


app.listen(3000, function() {
    console.log('Aplicación ejemplo, escuchando el puerto 3000!');
  });