async function nombre(id) {
  return new Promise(function (resolve, reject) {
    console.log(id)
    resolve({ nombre: 'Daniel', matricula: '00000' })
  })
}

async function direccion(id) {
  return new Promise((resolve, reject) => {
    resolve('leon gto 112')
  })
}

async function escuela(id) {
  return new Promise((resolve, reject) => {
    resolve('UTL')
  })
}

async function concatenar(id, nombre, matricula, direccion) {
  return new Promise((resolve, reject) => {
    if (id != 1) {
      reject('no se pudo')
    } else {
      resolve(
        `El ${id} del alumno ${nombre} con matricula ${matricula} y direccion ${direccion}`,
      )
    }
  })
}

async function funcionIntermedia(id) {
  return Promise.all([nombre(id), direccion(id), escuela(id)])
}

async function main() {
  let info = await funcionIntermedia(1)

  try {
    console.log(await concatenar(2, info[0].nombre, info[0].matricula, info[1]))
  } catch (error) {
    console.log(error)
  }

  // funcionIntermedia(1)
  //   .then((info) => {
  //     console.log(info)


  //     concatenar(1, info[0].nombre, info[0].matricula, info[1])
  //       .then((data) => {
  //         console.log(data)
  //       })
  //       .catch((error) => {
  //         console.log(error)
  //       }).finally(() => {
  //         console.log('hola')
  //       })

  //   })
  //   .catch((error) => {})



}

main()
