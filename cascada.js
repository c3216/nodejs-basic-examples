var async = require('async');

async.waterfall ([
     function( callback){ // crear un archivo 
        console.log ( "funcion uno ");
        callback(null); 
     },
     function( callback){ // leer unarchivo 
        console.log ( "funcion dos "); 
        // callback(null); // Todo bien 
        callback("chale ");
     },
     function( callback){ // borrar el archivo 
        console.log ( "funcion tres ");
        callback(null); 
     },
], function( error){
    if ( error == null )
         console.log ( "fin");
    else
        console.log( " error  " + error ); 
 }
); 