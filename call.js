function hola ( nombre , miCallback){
    setTimeout( function(){
        console.log( "hola ", nombre ); 
        miCallback (nombre);
    }, 1500); 
}

function hablar ( callbackHablar){
    setTimeout( function(){
        console.log( " platicando ... ciclo " ); 
        callbackHablar ();
    }, 1500); 
}


function adios ( nombre , otroCallback ){
    setTimeout( function(){
        console.log( " ADIOS  ", nombre ); 
        otroCallback ();
    }, 1800); 
}


function conversacion ( nombre, veces, callback){
    if ( veces > 0 ){
        hablar( function (){
            console.log( "Veces " + veces );
            conversacion( nombre, --veces , callback);
        }); 
    }
    else {
        adios( nombre, callback); 
    }
}

console.log ( "iniciando prtoceso ...");
hola ( "Azades! " , function( nombre ) {
    conversacion( nombre , 10, function(){
        console.log( "proceso terminado " ); 
    });
}); 