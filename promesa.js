async function hola ( nombre ){
    return new Promise ( function (resolve, reject ){
        setTimeout (function (){
            console.log(' funcion Hola ' + nombre );
            resolve (nombre);
        }, 1500);
    })
}


async function hablar ( nombre ){
    return new Promise ( function (resolve, reject ){
        setTimeout (function (){
            console.log(' bla bla ' );
            resolve (' algo');
        }, 1000);
    })
}


async function adios ( nombre ){
    return new Promise ( function (resolve, reject ){
        setTimeout (function (){
            console.log(' adios ' + nombre );
            resolve ();
        }, 1000);
    })
}

async function main () {
    let nombre = await hola ('Azades');
    hablar (); 
    hablar ();
    await hablar();
    await hablar ();
    await adios (nombre);
    console.log("Termina");
}


console.log("iniciamos" );
main();
console.log("segunda intuccion");  


